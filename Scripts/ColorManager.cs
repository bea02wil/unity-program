﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Управляет цветами прямоугольников. Конечно, команда Random.ColorHSV()
//в скрипте AppearOnClick генерирует случайный цвет из миллионов оттенков,
//но в теории может создаться одинаковый цвет и тогда слудующий метод его изменит
//на другой.
public class ColorManager : MonoBehaviour
{
    public Rectangle[] allRectangles;
    // Флаг для того, чтобы метод срабатывал только при создании
    //прямоугольника, а не каждый кадр. Меняет свое значение в классе AppearOnClick.
    public bool RectangleCreated { get; set; }
   
    void Update()
    {
        allRectangles = FindObjectsOfType<Rectangle>(); // Собирает найденные/удаленные прямоугольники в массив.
        CheckColors();
    }

    private void CheckColors()
    {
        if (RectangleCreated)
        {
            //Сравниваем цвета фигур друг с другом и если цвет какой-то из них равен содаваемому, то изменить цвет создаваемого
            //прямоугольника.
            for (int i = 0; i < allRectangles.Length - 1; i++)
            {
                for (int j = i + 1; j < allRectangles.Length; j++)
                {
                    if (allRectangles[i].GetComponent<SpriteRenderer>().color == allRectangles[j].GetComponent<SpriteRenderer>().color)
                    {
                        allRectangles[i].GetComponent<SpriteRenderer>().color = Random.ColorHSV();
                    }
                }
            }
            RectangleCreated = false;
        }
    }
}
