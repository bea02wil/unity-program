﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//Хранит различные объекты, среди которых два выделенных.
public class SelectedObjects : MonoBehaviour
{
    [SerializeField] ConnectionLine line;
    //Список временного хранилища двух выделенных объектов. На отпускание ctrl он очищается.    
    public List<GameObject> temporaryStorage = new List<GameObject>();
    //Глобальный список со вложенными списками. Хранит списки, где список это найденная связанная пара + связь.
    public List<List<GameObject>> connectedRecPairs = new List<List<GameObject>>();
    //Лист геймобъектов линий. Через него получим доступ к конкретной линии.
    public List<GameObject> lineRendererGameObjects = new List<GameObject>();
    public int HighLightedObjectsCount { get; set; } //Кол-во выделенных прямоугольников.   
    public bool ConnectionIsTrue { get; set; } = true;    
    public bool IsConnectedWithItself { get; set; } 
    
    //Добавляет во временное хранилище выделенный прямоугольник/ки.
    public void AddRecObjects(GameObject go)
    {
        temporaryStorage.Add(go);
        HighLightedObjectsCount++;
    }

    private void Update()
    {
        RestrictLineSpawnPlace();
        PreventItselfConnection();
    }

    //Предотвращет связывание с самим собой(от появления точки, хоть ее и не видно).
    //Если связано с самим собой, удаляем эту точку (связь).
    private void PreventItselfConnection()
    {
        if (IsConnectedWithItself)
        {
            for (int i = 0; i < lineRendererGameObjects.Count; i++)
            {
                if (lineRendererGameObjects[i] != null)
                {
                    if (lineRendererGameObjects[i].GetComponent<EdgeCollider2D>().points[0]
                        == lineRendererGameObjects[i].GetComponent<EdgeCollider2D>().points[1])
                    {
                        Debug.Log("Нельзя связать прямоугольник с самим собой"); //для эдитора
                        Destroy(lineRendererGameObjects[i]);                     
                    }
                }
            }
            IsConnectedWithItself = false;
        }
    }

    //Проверяет (по координатам коллайдера линий), если ли между прямоугольниками связь, если есть, удалить новую.
    private void RestrictLineSpawnPlace() 
    {
        if (ConnectionIsTrue)
        {
            if (lineRendererGameObjects.Count > 1)
            {
                for (int i = 0; i < lineRendererGameObjects.Count - 1; i++)
                {
                    for (int j = i + 1; j < lineRendererGameObjects.Count; j++)
                    {
                        if (lineRendererGameObjects[i] != null && lineRendererGameObjects[j] != null)
                        {
                            if (lineRendererGameObjects[i].GetComponent<EdgeCollider2D>().points[0]
                                == lineRendererGameObjects[j].GetComponent<EdgeCollider2D>().points[0]
                                        && lineRendererGameObjects[i].GetComponent<EdgeCollider2D>().points[1]
                                        == lineRendererGameObjects[j].GetComponent<EdgeCollider2D>().points[1])
                            {
                                Debug.Log("Нельзя добавить более одной связи между 1 и той же парой прямоугольников"); // для эдитора.
                                Destroy(lineRendererGameObjects[i]);                               
                            }
                        }
                    }
                }
            }
            ConnectionIsTrue = false;
        }
    }
}
