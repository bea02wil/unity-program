﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitProgram : MonoBehaviour
{
    [SerializeField] GameObject escMenu;
    bool escMenuOpened;

    private void Start()
    {
        escMenu.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            OpenCloseEscMenu();
        }
    }

    private void OpenCloseEscMenu()
    {
        if (!escMenuOpened)
        {
            OpenEscMenu(true);
        }
        else
        {
            OpenEscMenu(false);
        }
    }

    private void OpenEscMenu(bool status)
    {
        escMenu.SetActive(status);
        escMenuOpened = status;
    }

    public void OnButtonQuit()
    {
        Application.Quit();
    }
}
