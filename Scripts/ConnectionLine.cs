﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ConnectionLine : MonoBehaviour, IPointerClickHandler
{    
    SelectedObjects selectedObjects;
    AppearOnClick clickMe;
    LineRenderer lineRenderer;
    CursorSwitcher cursor;  
    EdgeCollider2D edgeCollider; 
    Vector3 firstPoint;
    Vector3 secondPoint;  
    int behindRectangle = 1;

    private void Start()
    {
        SetReferences();
    }

    private void SetReferences()
    {        
        clickMe = FindObjectOfType<AppearOnClick>();
        cursor = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CursorSwitcher>();
    }

    public void ConnectRectangles(GameObject firstPos, GameObject secondPos)
    {
        selectedObjects = FindObjectOfType<SelectedObjects>();

        GameObject newLine = Instantiate(gameObject, transform.position, transform.rotation);
        lineRenderer = newLine.GetComponent<LineRenderer>();
        //Ширина и высота линии.
        lineRenderer.startWidth = 0.05f;
        lineRenderer.endWidth = 0.05f;

        selectedObjects.lineRendererGameObjects.Add(newLine);
        edgeCollider = newLine.GetComponent<EdgeCollider2D>();

        GameObject rec1 = firstPos;
        GameObject rec2 = secondPos;
        Vector2[] recsVector = new Vector2[] { rec1.transform.position, rec2.transform.position };

        firstPoint = new Vector3(rec1.transform.position.x, rec1.transform.position.y, behindRectangle);
        secondPoint = new Vector3(rec2.transform.position.x, rec2.transform.position.y, behindRectangle);
        
        SetLinePositions();

        edgeCollider.points = recsVector; //Обновляем позицию коллайдера за позицией линии.      
    }

    //Установка первой и второй точек линии, между которыми ее провести.
    private void SetLinePositions()
    {
        lineRenderer.SetPosition(0, firstPoint);
        lineRenderer.SetPosition(1, secondPoint);
    }

    public void RedrawLine(GameObject rec1, GameObject rec2, EdgeCollider2D lineCollider)
    {
        foreach (GameObject go in selectedObjects.lineRendererGameObjects)
        {
            if (go != null && rec1 != null && rec2 != null)
            {
                if (lineCollider.GetInstanceID() == go.GetComponent<EdgeCollider2D>().GetInstanceID())
                {
                    lineRenderer = go.GetComponent<LineRenderer>();
                    
                    firstPoint = new Vector3(rec1.transform.position.x, rec1.transform.position.y, behindRectangle);
                    secondPoint = new Vector3(rec2.transform.position.x, rec2.transform.position.y, behindRectangle);

                    SetLinePositions();

                    //При перерисовке также обновляем коллайдер
                    edgeCollider = go.GetComponent<EdgeCollider2D>();                                       
                    edgeCollider.points = new Vector2[] { rec1.transform.position, rec2.transform.position};                   
                }
            }          
        }             
    }

    //Возвращаем геймобъект коллайдера, чтобы потом взять с него коллайдер
    //в Rectangle и передать его RedrawLine.
    public GameObject GetLineObject()
    {
        return edgeCollider.gameObject;
    }

    public void DestroyLine(GameObject line)
    {
        Destroy(line);
    }

    private void OnMouseOver()
    {
        clickMe.CursorNotOnLine = false;
        cursor.SetHandCursor();
    }

    private void OnMouseExit()
    {
        clickMe.CursorNotOnLine = true;
        cursor.SetMouseCursor();
    }

    public void OnPointerClick(PointerEventData eventData)
    {     
        if (eventData.clickCount == 2)
        {
            Destroy(gameObject);
            clickMe.CursorNotOnLine = true;
            cursor.SetMouseCursor();
        }
    }
}
