﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; //Пространство, позволяющее добавить интерфейс IPointerClickHandler

public class Rectangle : MonoBehaviour, IPointerClickHandler
{
    //Ссылки.
    [SerializeField] ConnectionLine line;
    [SerializeField] Sprite highLighting;
    SpriteRenderer sr;
    CursorSwitcher cursor;
    SelectedObjects selectedObjects;
    AppearOnClick dragMe;
    Vector2 offsetOfTouch;
    Rigidbody2D rb2D;    
    Color startColor;
    Sprite startSprite;     

    //Границы прямоугольника при перетаскивании.
    float minBoundX = 1.036f;
    float maxBoundX = 14.962f;
    float minBoundY = 0.479f;
    float maxBoundY = 11.522f;

    //Динамика вида курсоров, зажатия клавиши.
    bool isCtrlPressed;           
    bool dragging;    
    int currentObjectId; // id для this.gameObject.
  
    private void Start()
    {
        SetReferences();
    }

    //Устанавливем настройки при запуске.
    private void SetReferences()
    {
        //Привязываем объекты, находя их при запуске программы, чтобы работал функционал их классов, иначе будет null reference.
        dragMe = FindObjectOfType<AppearOnClick>();
        selectedObjects = FindObjectOfType<SelectedObjects>();
        cursor = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CursorSwitcher>();
        //Для удобочитаемости.
        rb2D = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        startColor = sr.color;
        startSprite = sr.sprite;
    }

    private void Update()
    {
        SelectRecOnCtrlKey();
        DestroyLineIfRecWasDeleted();
        PreventOutOfScreen();
        GetOffset();
        InvokeRedrawLine();
    }

    //Выделение на ctrl key.
    private void SelectRecOnCtrlKey()
    {
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            HighLightRecOn();
        }

        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            HighLightRecOff();
        }
    }

    //Если отпустить ctrl, то выделение пропадет.
    private void HighLightRecOff()
    {
        isCtrlPressed = false; //Запрещаем OnMouseDown срабатывать.

        //Возвращаем первоначальные цвет и спрайт. 
        sr.color = startColor;
        sr.sprite = startSprite;

        selectedObjects.temporaryStorage.Clear(); //Очищаем временный список выделенных объектов.
        selectedObjects.HighLightedObjectsCount = 0; //Устанавливаем 0 для дальнейших выделений.

        cursor.SetMouseCursor(); //Возвращаем прежний курсор мыши.
    }

    //При зажатом ctrl, если кликнуть, выделяется прямоугольник.
    private void HighLightRecOn()
    {
        //Флаг по которому срабатывает OnMouseDown, то есть клик над коллайдером.
        isCtrlPressed = true;
        cursor.SetSelectHandCursor(); //Смена курсора на руку.
    }

    //Проверяет, если в связанной паре удален один из прямоугольников,
    //то удалить и связь.
    private void DestroyLineIfRecWasDeleted()
    {
        if (selectedObjects.connectedRecPairs != null)
        {
            foreach (List<GameObject> eachPair in selectedObjects.connectedRecPairs)
            {
                foreach (GameObject go in eachPair)
                {
                    GameObject rec1 = eachPair[0];
                    GameObject rec2 = eachPair[1];
                    GameObject lineRenderer = eachPair[2];

                    if (rec1 == null || rec2 == null)
                    {
                        line.DestroyLine(lineRenderer);
                    }
                }
            }
        }
    }

    //Вычисляем смещение относительно позиции пивота (центр) прямоугольника,
    //чтобы при перетаскивании прямоугольник не прыгал на курсор, а тащился
    //оттуда, где взяли (так красивее как по мне).
    private void GetOffset()
    {
        if (Input.GetMouseButtonDown(0))
        {
            offsetOfTouch = (Vector2)transform.position - dragMe.OnMousePosition();
        }
    }

    //Не дать выйти части прямоугольника за экран и дать ему там остаться при перетаскивании со смещением.
    //Сделано через transform.position (а не
    //твердым телом, как в других случаях), чтобы избежать shaking при попытке уйти за экран. С помощью
    //transform не дергается.
    private void PreventOutOfScreen()
    {
        if (transform.position.x < minBoundX)
            transform.position = new Vector2(minBoundX, transform.position.y);

        if (transform.position.x > maxBoundX)
            transform.position = new Vector2(maxBoundX, transform.position.y);

        if (transform.position.y < minBoundY)
            transform.position = new Vector2(transform.position.x, minBoundY);

        if (transform.position.y > maxBoundY)
            transform.position = new Vector2(transform.position.x, maxBoundY);              
    }

    //Срабатывает при удержании левой кнопки мыши.
    private void OnMouseDrag()
    {            
        MoveRectangle();              
    }
  
    //Проходимся по списку связанных прямоугольников списком каждой пары
    //и если в паре совпадет id прямоугольников с текущим взятым, то
    //перерисовать их связь по средствам передачи в метод коллайдера
    //линии. Метод находится в Update, потому что если привязать его относительно
    //OnMouseDrag, то, если кинуть резко связанный прямоугольник, то связь на большой скорости
    //не успевает перерисовываться за ним и отрывается. Нужно больше кадров обновления, поэтому
    //помещаем его в Update.
    private void InvokeRedrawLine()
    {
        if (selectedObjects.connectedRecPairs != null)
        {
            foreach (List<GameObject> eachPair in selectedObjects.connectedRecPairs)
            {
                foreach (GameObject go in eachPair)
                {
                    GameObject rec1 = eachPair[0];
                    GameObject rec2 = eachPair[1];
                    GameObject lineRenderer = eachPair[2];

                    if (rec1.GetInstanceID() == currentObjectId || rec2.GetInstanceID() == currentObjectId)
                    {
                        if (lineRenderer != null)
                        {
                            line.RedrawLine(rec1, rec2, lineRenderer.GetComponent<EdgeCollider2D>());
                        }
                    }
                }
            }
        }
    }

    //Срабатывает, если отпустить клик.
    private void OnMouseUp()
    {
       //Как только отпустили клик при перемещении, объект снова становится
       //Kinematic, чтобы через него нельзя было пройти.
       rb2D.isKinematic = true;      
       dragging = false;
    }

    //Срабытывает на выходе курсора с прямоугольника.
    private void OnMouseExit()
    {
        SetActiveLineCollider(true);
        ChangeCursorToDefolt();
        ChangeCursorToHandIfCtrlKeyDown();
    }

    private void ChangeCursorToHandIfCtrlKeyDown()
    {
        if (isCtrlPressed)
        {
            cursor.SetSelectHandCursor();
        }
    }

    private void ChangeCursorToDefolt()
    {
        if (!dragging)
        {
            cursor.SetMouseCursor();
        }
    }

    //Срабатывает при наведении курсора на прямоугольник.
    private void OnMouseOver()
    {
        SetActiveLineCollider(false);
        ChangeCursorToMove();
    }

    private void ChangeCursorToMove()
    {
        if (!isCtrlPressed)
        {
            cursor.SetMoveCursor();
        }
    }

    //Включить/выключить коллайдер линии. Нужно для того, чтобы избежать той проблемы, когда
    //берешься перетаскивать прямоугольник в месте где под ним линия, то за него попросту 
    // нельзя было взяться.
    private void SetActiveLineCollider(bool status)
    {
        if (selectedObjects.lineRendererGameObjects != null)
        {
            foreach (GameObject line in selectedObjects.lineRendererGameObjects)
            {
                if (line != null)
                {
                    line.GetComponent<EdgeCollider2D>().enabled = status;
                }
            }
        }
    }   
    
    private void MoveMeAndBeDynamic(Vector2 offset)
    {
        rb2D.isKinematic = false;
        rb2D.MovePosition(dragMe.OnMousePosition() + offset);
    }

    //Собственно, перемещать прямоугольник.   
    private void MoveRectangle()
    {         
          currentObjectId = gameObject.GetInstanceID(); //Получаем Id передвигаемого прямоугольника.
          dragging = true;

          //Из-за того, что перемещение реализовано со смещением, а границы были ограченичены в методе
          //OnMousePosition класса AppearOnClick без смещения, то, если бы без этого блока взяться, к примеру, в левой стороне
          //прямоугольника и вести к левой границе, то прямоугольник не доходил бы до нее. Этот блок if устраняет это.
          if (dragMe.OnMousePosition().x == minBoundX || dragMe.OnMousePosition().x == maxBoundX
             || dragMe.OnMousePosition().y == minBoundY || dragMe.OnMousePosition().y == maxBoundY)
          {
             Vector2 noOfset = Vector2.zero;
             MoveMeAndBeDynamic(noOfset);                             
          }
          
          //Здесь в и блоке If первая строка отвечает, что при перемещении наше тело становится Dynamic.
          //Изначально оно Kinematic. Делается это затем, чтобы объекты могли сталкиваться, то есть при
          //Dynamic и Kinematic. Перемещение делалось "физикой", чтобы объекты видели друг друга при столкновении,
          //в отличие от transform, где физика игнорируется и объекты проходят сквозь друг друга.
          else
          {
             MoveMeAndBeDynamic(offsetOfTouch);                      
          }                    
    }

    //Срабатывает при кликах, в данном случае при двух. Для того, чтобы работало в этом скрипте, был
    //добавлен скрипт Physics 2D Raycaster на камеру.
    public void OnPointerClick(PointerEventData eventData)
    {       
        if (eventData.clickCount == 2)
        {
            Destroy(gameObject);
        }
    }

    //Срабытывает на клик мыши.
    private void OnMouseDown()
    {
        if (isCtrlPressed && selectedObjects.HighLightedObjectsCount <= 2)
        {
            selectedObjects.AddRecObjects(gameObject);

            sr.color = new Color32(255, 255, 255, 255);
            sr.sprite = highLighting;         

            if (selectedObjects.HighLightedObjectsCount == 2)
            {
                AddTheseRecsToGlobalStorage();
            }
        }
        else if (selectedObjects.HighLightedObjectsCount > 2)
            Debug.Log("Соединять можно только пару объектов за одно удержание ctrl"); //Для эдитора.
    }

    private void AddTheseRecsToGlobalStorage()
    {
        GameObject rec1, rec2;
        rec1 = selectedObjects.temporaryStorage[0];
        rec2 = selectedObjects.temporaryStorage[1];

        line.ConnectRectangles(rec1, rec2); //Создаем связь.

        selectedObjects.ConnectionIsTrue = true;
        selectedObjects.IsConnectedWithItself = true;

        //Перехватывающий список списка элемнтов временного хранилища плюс геймобъекта линии.
        List<GameObject> recsAndLine = new List<GameObject>() { rec1, rec2, line.GetLineObject() };

        //Добавляем перехватывающий список в глобальное хранилище прямоугольников с их связями.
        selectedObjects.connectedRecPairs.Add(recsAndLine);
        selectedObjects.HighLightedObjectsCount++;
    }
}
