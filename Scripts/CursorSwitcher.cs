﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Переключатель курсоров, весит на камере.
public class CursorSwitcher : MonoBehaviour
{
    [SerializeField] Texture2D handCursor;
    [SerializeField] Texture2D moveCursor;
    [SerializeField] Texture2D selectHandCursor;
    //Оффсеты для каждого из курсора, 
    //чтобы совпали с координатами точки срабатывания обычного курсора Windows.
    //В инспекторе под каждую текстуру макс. размер установлен в 32 пикселя.
    Vector2 hotSpotMoveCursor = new Vector2(16, 16);
    Vector2 hotSpotHandCursor = new Vector2(10, 0);
    Vector2 hotSpotMouseCursor = Vector2.zero;
    CursorMode cursorMode = CursorMode.Auto;

    public void SetHandCursor()
    {        
        Cursor.SetCursor(handCursor, hotSpotHandCursor, cursorMode);
    }

    public void SetMoveCursor()
    {        
        Cursor.SetCursor(moveCursor, hotSpotMoveCursor, cursorMode);       
    }

    public void SetMouseCursor()
    {
        Cursor.SetCursor(null, hotSpotMouseCursor, cursorMode);
    }

    public void SetSelectHandCursor()
    {
        Cursor.SetCursor(selectHandCursor, hotSpotHandCursor, cursorMode);
    }
}
