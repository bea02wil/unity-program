﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Предотвращает перекрытие при добавлении прямоугольников друг на друга.
public class DetectOverlap : MonoBehaviour
{
    //Создаем  массив коллайдеров для дальнейшего сбора всех найденных коллайдеров в сцене.
    public Collider2D[] colliders;
    //Маска только для перекрытия прямоугольников. В инспектор добавлен слой "Rectangle" и выбран у прямоугольников.
    public LayerMask mask;  
    public float radius;   

    //Собственно, сам метод обнаружения перекрытия, вызывается в классе AppearOnClick.
    public bool CheckOverlap(Vector2 spawnPoint)
    {
       //Получаем список коллайдеров, которые попадают в круг с позицией, равной this.gameObject
       //в инспекторе(0, 0, 0) и радиусом 20 в инспекторе для полного перехвата в текущей сцене.
       colliders = Physics2D.OverlapCircleAll(transform.position, radius, mask);
       //Создаем цикл коллайдеров, найденных в процессе клика мышью.
       for (int i = 0; i < colliders.Length; i++)
       {
           //Находим центр круга.
           Vector2 centerColliderPoint = colliders[i].bounds.center;           
           //Значения, чтобы расширить перекрывающий круг для избежания перекрывания вне прямоугольника.
           float expandCircleX = 1.04f;
           float expandCircleY = 0.48f;
           //Два значения half. Находим половину длины и высоты круга, т.к extents отвечает за
           //за половину размера - т.е. bounds.size. Но так, как круг немного расширяем, то
           //находим  не половины, а чуть больше половины. Круг расширили, чтобы в области
           //рядом с прямоугольником так же предотвратить  перекрытие.
           float halfWidthCollider = colliders[i].bounds.extents.x + expandCircleX;
           float halfHeightCollider = colliders[i].bounds.extents.y + expandCircleY;
           //Min и max значения точек круга по X и Y.
           float minXpointCollider = centerColliderPoint.x - halfWidthCollider;
           float maxXpointCollider = centerColliderPoint.x + halfWidthCollider;
           float minYpointCollider = centerColliderPoint.y - halfHeightCollider;
           float maxYpointCollider = centerColliderPoint.y + halfHeightCollider;
           //И, собственно, проверяем, входит ли создаваемый прямоугольник (а точнее
           //точка клика) в круг или нет,
           //если входит, тогда на месте клика уже есть прямоугольник и текущий не создается,
           if (spawnPoint.x >= minXpointCollider && spawnPoint.x <= maxXpointCollider)
           {
               if (spawnPoint.y >= minYpointCollider && spawnPoint.y <= maxYpointCollider)
                   return false;
           }
       }
       //если не входит, то создать.
       return true;
    } 
}
