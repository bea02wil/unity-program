﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Добавляет прямоугольники в сцену по нажатию мыши.
public class AppearOnClick : MonoBehaviour
{   
    //Ссылки. 
    [SerializeField] Rectangle rectangle;
    [SerializeField] ColorManager colorManager;
    SpriteRenderer generateColor;
    DetectOverlap detectOverlap;                   
    //Флаг для проверки, если щелчок был на связи(линии), то не создавать прямоугольник.
    //Это для того, чтобы по клику (двойному) была реакция на связь, чтобы ее удалить.
    public bool CursorNotOnLine { get; set; } = true;      

    private void Start() //Метод, который первый загружается, срабытывает один раз при запуске программы. (Awake-самый первый)
    {
        SetReferences();
    }

    private void SetReferences()
    {
        generateColor = rectangle.GetComponent<SpriteRenderer>();
        detectOverlap = FindObjectOfType<DetectOverlap>();
    }

    private void Update() //Метод, который обрабатывает содержимое каждый кадр(frame).
    {
        OnMousePosition();
        AddRectangleOnMouseClick();
    }

    //Добавляет прямоугольник со своим цветом по нажатию левой кнопки мыши.
    private void AddRectangleOnMouseClick()
    {
        if (Input.GetMouseButtonDown(0))
        {
            AddRecWithOwnColor();
        }
    }

    private void AddRecWithOwnColor()
    {
        //Проверяем, если в месте клика нет прямоугольника и курсор не на связи, тогда его создать.
        bool areaIsEmpty = detectOverlap.CheckOverlap(OnMousePosition());
        if (areaIsEmpty && CursorNotOnLine)
        {
            //Генерируем случайный цвет.
             generateColor.color = Random.ColorHSV();                  
            //Производим появление префаба-прямоугольника в месте клика мышью.
            Instantiate(rectangle, OnMousePosition(), rectangle.transform.rotation);
            //Флаг, чтобы метод Update класса ColorManager срабатывал только, если добавлен прямоугольник.
            colorManager.RectangleCreated = true;
        }
        else
        {
            //Для эдитора, чтобы были пояснения действий.
            Debug.Log("Невозможно добавить прямоугольник в этом месте во избежание перекрытия");
        }
            
    }

    //Считываем координаты указателя мыши, чтобы в месте его клика добавить прямоугольник.
    public Vector2 OnMousePosition()
    {
        //Ширина и высота текущей сцены, размер камеры равен 6.
        float gameWidthInUnits = 16;
        float gameHeightInUnits = 12;
        //Координаты указателя без периметрового ограничения.
        //Получаем координаты указателя мыши в режиме Game.
        float mouseCoordinateX = Input.mousePosition.x / Screen.width * gameWidthInUnits;
        float mouseCoordinateY = Input.mousePosition.y / Screen.height * gameHeightInUnits;       
        //Ограничивающие переменные создания прямоугольников по периметру, чтобы они
        //не уходили за экран.
        float minBoundX = 1.036f;
        float maxBoundX = 14.962f;
        float minBoundY = 0.479f;
        float maxBoundY = 11.522f;
        //Координаты указателя с периметровым ограниченим.
        //Ограничиваем создание прямоугольников во избежание их ухода за экран.
        float restrictedMousePosX = Mathf.Clamp(mouseCoordinateX, minBoundX, maxBoundX);
        float restrictedMousePosY = Mathf.Clamp(mouseCoordinateY, minBoundY, maxBoundY);
        //Точка создания прямоугольника.
        //Получаем нужные координаты там, где будет произведен клик.
        Vector2 onMouseClickPos = new Vector2(restrictedMousePosX, restrictedMousePosY);               
        return onMouseClickPos;
    }
}
